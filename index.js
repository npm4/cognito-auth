"use strict";

// package imports
const axios = require('axios')
const jwt = require('jsonwebtoken')
const jwktp = require('jwk-to-pem')
const ntlm = require('ntlm-client')
const { parse } = require('node-html-parser')

// add cookiejar support
const axiosCookieJarSupport = require('@rand/axios-cookiejar-support').default;
const tough = require('tough-cookie');
axiosCookieJarSupport(axios);

// node library imports
const querystring = require('querystring')
const https = require('https')

exports.oauthResponse = async (cognitoData) => {
  try {
    /*
     *
     * State (CSRF) verification is being removed temporarily because
     * of issues with load balancing. A user's login request is not guaranteed
     * to return to the same instance, thus certainty of the req.app property 'state'
     * is in doubt - ie, potential it's not set, so it's possible that the
     * login could fail due to the verification failing.

     * When issues with load balancing have been investigated further, this could potentially go back in.
    const appState = req.app.get('state')
    const reqState = req.query['state']


    console.log("STATE VALIDATION:")
    console.log('appState', appState)
    console.log('reqState', reqState)
    // If there is no state value to validate or it does not match what was set
    // when the request began, the request is invalid.
    if (appState === undefined || reqState !== appState) {
      throw new Error('Invalid state')
    }
    */

    const data = querystring.stringify({
      grant_type: 'authorization_code',
      client_id: cognitoData.client_id,
      redirect_uri: cognitoData.redirect_uri,
      code: cognitoData.code
    })

    console.log('data', data)

    // Send request to Cognito to validate the authorization_code provided
    // to /oauth/response
    // Returns all three tokens: access, id, and refresh
    const tokenResponse = await axios({
      url: `https://${cognitoData.user_pool}.auth.us-west-2.amazoncognito.com/oauth2/token`,
      method: 'post',
      data: data
    })

    const idToken = await decodeCognitoToken(tokenResponse.data['id_token'], cognitoData.pool_id)

    return { status: 200, message: 'Token Found', token: idToken }

  } catch (err) {
    console.error('err', err)
    console.error('err.stack', err.stack)
    console.error('err.message', err.message)
    return { status: 500, message: 'Invalid data', token: null}
  }
}

let decodeCognitoToken = async(token, pool_id) => {
  try {
    // Gets signing keys from AWS
    var jwkUrl = `https://cognito-idp.us-west-2.amazonaws.com/${pool_id}/.well-known/jwks.json`
    const jwkResponse = await axios.get(jwkUrl)

    const decodedToken = jwt.decode(token, { complete: true })
    const kId = decodedToken.header.kid

    const signingJwk = jwkResponse.data.keys.find((key) => {
      return key.kid == kId
    })

    // Convert the signing key to a PEM
    const signingPem = jwktp(signingJwk)

    // Validate the token with the PEM that it was 'signed' with.
    const verified = await jwt.verify(token, signingPem, {})
    if (verified) {
      return decodedToken
    } else {
      throw new Error('Unable to decode token')
    }
  } catch (err) {
    throw new Error('Unable to decode token')
  }
}

// This method is used to generate a CSRF Token that is passed through the
// the Cognito authentication method
const generateVerification = () => {
    var verification = ''
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    for (var i = 0; i < 32; i++) {
      verification += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    return verification
}

exports.getCognitoToken = async (cognitoData) => {
  const cookieJar = new tough.CookieJar();
  const type1msg = ntlm.createType1Message();
  const URL = `https://${cognitoData.user_pool}.auth.us-west-2.amazoncognito.com/oauth2/authorize`
  const keepAliveAgent = new https.Agent({ keepAlive: true })

  try {
    // Not setting or sending state at this point, due to load balancing issues.
    // const state = generateVerification()
    // req.app.set('state', state)

    const initResponse = await axios.get(URL, {
        headers: {
          'User-Agent': cognitoData.user_agent,
          'Authorization': type1msg,
        },
        params: {
          identity_provider: 'RAND-ADFS',
          client_id: cognitoData.cognito.client_id,
          redirect_uri: cognitoData.cognito.redirect_uri,
          response_type: 'CODE',
          scope: 'openid'
        },
        httpsAgent: keepAliveAgent,
        validateStatus: (status) => {
          return (status >= 200 && status <= 302 || status == 401)
        },
        jar: cookieJar,
        withCredentials: true,
    })

    const type2msg = ntlm.decodeType2Message(initResponse.headers['www-authenticate'])
    const type3msg = ntlm.createType3Message(type2msg, cognitoData.id, cognitoData.pwd)

    const responseUri = initResponse.request.path

    console.log('authResponse START')

    const authResponse = await axios.get(`https://adfs.rand.org${responseUri}`, {
      headers: {
        'User-Agent': cognitoData.user_agent,
        'Authorization': type3msg,
      },
      httpsAgent: keepAliveAgent,
    })

    console.log('authResponse OK')

    const root = parse(authResponse.data)
    const samlResponse = root.querySelector("[name=SAMLResponse]")._attrs.value
    const relayState = root.querySelector("[name=RelayState]")._attrs.value

    const data = querystring.stringify({
      RelayState: relayState,
      SAMLResponse: samlResponse
    })

    console.log('idpResponse START')

    const idpResponse = await axios.post(
      `https://${cognitoData.user_pool}.auth.us-west-2.amazoncognito.com/saml2/idpresponse`,
      data,
      {
        httpsAgent: keepAliveAgent,
        jar: cookieJar,
        validateStatus: (status) => {
          return status >= 200 && status <= 302
        },
        withCredentials: true,
      }
    )

    console.log('idpResponse OK')

    const idToken = idpResponse.data['id_token']
    console.log('idToken', idToken)

    return {
      status: 200,
      message: 'Token found',
      token: idToken
    }

  } catch (err) {
    console.error('err', err)
    console.error('err.stack', err.stack)
    return {
      status: 200,
      message: 'Token not found',
      token: null
    }
  }
};

exports.checkCognitoGroup = (profile, FOUO_GROUP) => {
    // Remove brackets from 'profile' key in idToken.payload, it comes in as a
    // string wrapped in [], so have to strip and convert string to array.
    const groupString = profile.replace(/\[/, '').replace(/\]/, '')

    // Attempt to split groups and check that `HR Access Level - Full` exists
    // in the group array.
    try {
      // Convert group list from string separated by ', ' to an array.
      const groupArr = groupString.split(', ')
      if (groupArr.includes(FOUO_GROUP)) {
        return true
      }
    } catch (err) {
      console.error(err)
      console.error(err.stack)
      console.warn(`Unable to create groupArr for checking '${FOUO_GROUP}' membership.`)
      return false
    }

    return false
};