# RAND Cognito Auth Package

Auth package to abstract current get cognito token, logout, and oauth response token workflows

## Features

- Provides getCognitoToken and oauthResponse functions to be called using request supplemented function parameters
- Provides logout function to be called using traditional request parameters

## Installing

Using npm:

```bash
$ npm install git+ssh://git@code.rand.org:rand-npm/cognito-auth.git
```

## Docs

### getCognitoToken (async function)

PARAMS
{
    [ Cognito ID ]
    [ Cognito PWD ]
    [ User Agent ]
    [ User Pool ]
    {
        [ Client ID ]
        [ Redirect URI ]
    }
}

RESPONSE
{
    [ status ]
    [ message ]
    [ token ]
}

### logout

PARAMS
[ Request ]
[ Response ]

RESPONSE
[ String ]

### oauthResponse (async function)

PARAMS
{
    [ Code ]
    [ Client ID ]
    [ Pool ID ]
    [ Redirect URI ]
    [ User Pool ]
}

RESPONSE
{
    [ status ]
    [ message ]
    [ token ]
}